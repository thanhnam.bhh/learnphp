<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li>
        <a href="/administrator">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Danh mục sản phẩm</span>
        <span class="pull-right-container">
        <span class="label label-primary pull-right">2</span>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/administrator?action=product_categories/add"><i class="fa fa-circle-o"></i> Thêm danh mục</a></li>
            <li><a href="/administrator?action=product_categories/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        </ul>
    </li>
</ul>