<?php  
	if( isset($_POST['submit']) ) {
		if( !empty($_POST['title']) && !empty($_POST['slug']) && !empty($_POST['description']) ) {
			$title = $_POST['title'];
			$slug = $_POST['slug'];
			$des = $_POST['description'];
			$sql = "INSERT INTO product_categories (title, slug, description) VALUES ('$title', '$slug', '$des')";
			// Thực hiện câu lệnh
			if (mysqli_query($conn, $sql)) {
			    echo "<script>";
                echo "alert('Thêm danh mục sản phẩm thành công!');";
                echo "window.location.href = '/administrator?action=product_categories/list';";
                echo "</script>";
			} else {
				echo "<script>";
                echo "alert('Lỗi: ".mysqli_error($conn)."');";
                echo "</script>";
			}
		}
	}

?>

<section class="content-header">
    <h1>
        Thêm danh mục sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm danh mục sản phẩm</li>
    </ol>
</section>

<section class="content">
	<form action="" method="POST">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Tiêu đề</label>
					<input type="text" class="form-control" name="title" required>
				</div>
				<div class="form-group">
					<label>Tên không dấu</label>
					<input type="text" class="form-control" name="slug" required>
				</div>
				<div class="form-group">
					<label>Mô tả</label>
					<textarea name="description" required class="form-control"></textarea>
				</div>
				<div class="text-right">
					<button class="btn btn-primary" type="submit" name="submit">Thêm</button>
					<button class="btn btn-default" type="reset">Reset</button>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
	</form>
		
</section>