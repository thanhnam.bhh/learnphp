
<section class="content-header">
    <h1>
        Danh sách danh mục sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách danh mục sản phẩm</li>
    </ol>
</section>
<section class="content">
	<div class="box-body">
	    <table id="example2" class="table table-bordered table-hover">
	        <thead>
	            <tr>
	                <th>STT</th>
	                <th>Tên</th>
	                <th>Mô tả</th>
	                <th></th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php  
	        		$sql = "SELECT * FROM product_categories ORDER BY id DESC";
	        		$result = mysqli_query($conn, $sql);
	        		if (mysqli_num_rows($result) > 0) {
	        			$i = 1;
	        			while ( $row = mysqli_fetch_assoc($result) ) {
	        	?>
	        				<tr>
				                <td><?php echo $i; ?></td>
				                <td><?php echo $row['title']; ?></td>
				                <td><?php echo $row['description']; ?></td>
				                <td>
				                	<a href="#" class="btn btn-success" title="Sửa">Sửa</a>
				                </td>
				                <td>
				                	<a href="#" class="btn btn-danger" title="Xóa">Xóa</a>
				                </td>
				            </tr>
	        	<?php
	        				$i++;
	        			}
	        		}
	        	?>
	            
	            
	        </tbody>
	    </table>
	</div>
</section>